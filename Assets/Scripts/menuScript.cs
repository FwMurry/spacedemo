﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

/**
Code to handle Start Menu
*/

public class menuScript : MonoBehaviour {

    public Canvas quitMenu;
    public Button playButton;
    public Button quitButton;
	// Use this for initialization
	void Start () {
        quitMenu = quitMenu.GetComponent<Canvas>();
        playButton = playButton.GetComponent<Button>();
        quitButton = quitButton.GetComponent<Button>();
        quitMenu.enabled = false;
    }
	
    /**
    When User Clicks on Quit at Start Menu
    */
    public void QuitClick()
    {
        quitMenu.enabled = true;
        playButton.enabled = false;
        quitButton.enabled = false;
    }

    /**
    When No on Quit Menu is clicked
    */
    public void NoClick()
    {
        quitMenu.enabled = false;
        playButton.enabled = true;
        quitButton.enabled = true;
    }

    /**
    User Clicks on Play start the level
    */
    public void StartLevel()
    {
        SceneManager.LoadScene(1);
    }
    /**
    User Quit the game
    */
    public void ExitGame()
    {
        Application.Quit();
    }
}
