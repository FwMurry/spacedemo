﻿using UnityEngine;
using System.Collections;

public class astroidPhysics : MonoBehaviour {
    public Transform asteroid;
    public float speed = 5f;

	// Update is called once per frame
	void Update () {
        float x = Random.Range(0f, 3f);
        float y = Random.Range(0f, 3f);
        float z = Random.Range(0f, 3f);

        transform.Rotate(x*Time.deltaTime, y*Time.deltaTime, z* Time.deltaTime);
	}
}
